import './App.css';
import TopNews from "./components/news/TopNews";
import Navbar from "./components/navbar/Navbar";
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import NotFound from "./components/NotFound";
import NewsArticle from "./components/article/NewsArticle";

function App() {
  return (
      <div className="container p-0 container-border mt-5 bg-light">
        <Router>
            <Navbar/>
            <Switch>
                <Redirect exact from='/' to='/topNews/country/gb'/>
                <Redirect exact from='/topNews' to='/topNews/country/gb'/>
                <Redirect exact from='/search' to='/search/country/gb'/>
                <Redirect exact from='/categories' to='/categories/category/all/country/gb'/>
                <Redirect exact from='/categories/category' to='/categories/category/all/country/gb'/>

                <Route exact path="/topNews/country/:country"><TopNews/></Route>
                <Route exact path="/topNews/article"><NewsArticle/></Route>
                <Route exact path="/categories/category/:category/country/:country"><TopNews/></Route>
                <Route exact path="/search/country/:country"><TopNews/></Route>
                <Route path="*"><NotFound/></Route>
            </Switch>
        </Router>
      </div>
  );
}

export default App;
