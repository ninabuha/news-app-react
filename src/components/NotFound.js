import React from "react";
import {Link} from "react-router-dom";
import "../css/NotFound.css";

const NotFound = () =>  {
    return (
        <div className="container">
            <div className="jumbotron">
                <h2 className="mb-4 oops">Ooooops!</h2>
                <h3 className="mb-4">Page Not Found!</h3>
                <p>This page does not exist!</p>
                <Link to="/topNews">Back to Home page</Link>
            </div>
        </div>
    )
}

export default NotFound;