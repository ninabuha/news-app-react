import React, {useEffect, useState} from "react";
import {Link, useLocation} from "react-router-dom";
import "../../css/Article.css";
import noImageImage from '../../data/no-image.jpg';

const NewsArticle = () =>   {
    const location = useLocation();
    const {title, content, urlToImage} = location.state.article;
    const [backLink, setBackLink] = useState("/topNews");

    useEffect(() => {
        setBackLink(location.state.prevPath);
    }, [location]);

    return (
        <div className="container mb-3">
            <ul>
                <li><h4>{title}</h4></li>
            </ul>
            <div className="m-auto w-100">
                <div className="card-body">
                    <img className="card-img-top" src={urlToImage || noImageImage} alt={title} />
                    {content && <div className="border p-3 mt-3"><p className="card-description">{content}</p></div>}
                    <Link to={backLink}><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/></svg>
                        Back to List</Link>
                </div>
            </div>
        </div>
    )
}

export default NewsArticle;