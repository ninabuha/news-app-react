import React from "react";
import {NavLink, useLocation} from "react-router-dom";
import "../../css/Article.css";
import noImageImage from '../../data/no-image.jpg';

const NewsArticleCard = (props) =>   {
    const location = useLocation();
    const {title, description, urlToImage} = props.article;

    return (
        <div className="container mb-3">
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>
                    <img className="card-img-top" src={urlToImage || noImageImage} alt={title} />
                    <p className="card-description mt-2">{description}</p>
                    <NavLink to={{ pathname: "/topNews/article", state: { article: props.article, prevPath: location.pathname}}} className="ml-auto">More
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/></svg></NavLink>
                </div>
            </div>
        </div>
    )
}

export default NewsArticleCard;