import React from "react";
import "../../css/Categories.css";

const CategorySlider = (props) =>   {
    return (
        <div className="container">
            <ul>
                <li><h4>{props.category}</h4></li>
            </ul>
            <CategorySlider article={props.article} />
        </div>
    )
}

export default CategorySlider;