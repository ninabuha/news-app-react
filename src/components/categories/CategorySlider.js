import React from "react";
import "../../css/Categories.css";
import NewsArticleCard from "../article/NewsArticleCard";
import {NavLink} from "react-router-dom";
import "../../css/CategorySlider.css";

const CategorySlider = (props) =>   {
    const articles = props.articles;
    const category = props.category;

    return (
        <div className="container align-content-center mt-4">
            <ul>
                <li><h4><NavLink to={`/categories/category/${category}/country/${props.counrty}`} className="text-dark h3 text-capitalize text-decoration-none">{category}
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/></svg></NavLink></h4></li>
            </ul>

            {articles && articles.length > 0 && <div>
                <div id={category} className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target={`#${category}`} data-slide-to="0" className="active"/>
                        <li data-target={`#${category}`} data-slide-to="1"/>
                    </ol>

                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <div className="row container mt-4 mb-4">
                                <div className="col-lg-4 col-md-12 col-sm-12">
                                    {articles && articles[0] && <NewsArticleCard article={articles[0]}/>}
                                </div>
                                <div className="col-lg-4 col-md-12 col-sm-12">
                                    {articles && articles[1] && <NewsArticleCard article={articles[1]}/>}
                                </div>
                                <div className="col-lg-4 col-md-12 col-sm-12">
                                    {articles && articles[2] && <NewsArticleCard article={articles[2]}/>}
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className="row container mt-4 mb-4">
                                <div className="col-lg-4 col-md-12 col-sm-12">
                                    {articles && articles[3] && <NewsArticleCard article={articles[3]}/>}
                                </div>
                                <div className="col-lg-4 col-md-12 col-sm-12">
                                    {articles && articles[4] && <NewsArticleCard article={articles[4]}/>}
                                </div>
                            </div>
                        </div>
                    </div>
                    {articles && articles.length > 3 && <>
                        <a className="carousel-control-prev" href={`#${category}`} data-slide="prev">
                            <span className="carousel-control-prev-icon"/>
                        </a>
                        <a className="carousel-control-next" href={`#${category}`} data-slide="next">
                            <span className="carousel-control-next-icon"/>
                        </a>
                    </>}
                </div>
            </div>}

            {!articles && <p>There is currently no available articles for this category.</p>}
        </div>
    )
}

export default CategorySlider;