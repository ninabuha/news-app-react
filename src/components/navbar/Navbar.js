import React, {useEffect, useState} from "react";
import {NavLink, useParams} from "react-router-dom";
import { useLocation } from 'react-router-dom';
import "../../css/Nav.css";
import NavbarToggler from "./NavbarToggler";
import {countries, mainMenuItems} from "../../routesAndNavigation/navigation";

const [topNews, categories, search] = mainMenuItems;

const Navbar = () =>    {
    const location = useLocation();
    const urlParams = useParams();
    const [countryClass, setCountryClass] = useState("nav-link");
    const [navLink, setNavLink] = useState(topNews.url);
    const [gb, us] = countries;

    useEffect(() => {
        setCountryClass(location.pathname === "/topNews/article" ? "nav-link nav-link-orange disabled"   :   "nav-link");

        if (location.pathname.includes("topNews")) {
            setNavLink(topNews.url);
        } else if (location.pathname.includes("search")) {
            setNavLink(search.url);
        } else if (location.pathname.includes("categories")) {
            setNavLink(categories.url);
        }
    }, [location.pathname, urlParams]);

    const getUrlForCountry = () =>  {
        return location.pathname.includes(gb.country) ? gb.url : location.pathname.includes(us.country) ? us.url : gb.url;
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-light p-0 mb-4">
            <NavbarToggler />

            <div className="collapse navbar-collapse" id="menu">
                <ul className="navbar-nav mr-auto">
                    {mainMenuItems.map((menuItem) =>    {
                        return (
                            <li className="nav-item" key={menuItem.url}>
                                <NavLink to={`${menuItem.url}${getUrlForCountry()}`} className="nav-link" activeClassName="active">{menuItem.name}</NavLink>
                            </li>
                        )
                    })}
                </ul>
                <ul className="navbar-nav ml-auto">
                    {countries.map((c, id) =>   {
                        return (
                            <li className="item-right" key={id}>
                                <NavLink to={`${navLink}${c.url}`} className={countryClass} activeClassName="active">{c.country.toUpperCase()}</NavLink>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </nav>
    )

}

export default Navbar;