import React from "react";

const Search = (props) =>   {
    return (
        <div className="container mb-3">
            <form>
                <div className="form-group">
                    <input type="text" className="form-control" id="searchValue" name="searchValue" placeholder="Search Term..."
                           value={props.value}
                           onChange={props.onChangeInput}/>
                </div>
            </form>
        </div>
    )
}

export default Search