import React, {useEffect, useState} from "react";
import {useLocation, useParams} from "react-router-dom";
import NewsArticleCard from "../article/NewsArticleCard";
import {useFetch} from "../customHooks/useFetch";
import Search from "./Search";
import {
    apiKey,
    entertainment,
    gbNewsText,
    gbSearchText,
    general,
    health,
    newsapiUrl,
    science,
    sport,
    technology,
    usNewsText,
    usSearchText
} from "../../textsAndApiInfo/texts_and_api_info";
import {countries} from "../../routesAndNavigation/navigation";
import CategorySlider from "../categories/CategorySlider";

const [gb, us] = countries;

const TopNews = () =>   {
    const urlParams = useParams();
    const location = useLocation();
    const [country, setCountry] = useState(gb.country);
    const [topNewsText, setTopNewsText] = useState(gbNewsText);
    const [url, setUrl] = useState(`${newsapiUrl}country=${country}&apiKey=${apiKey}`);
    const {loading, data} = useFetch(url);
    const [search, setSearch] = useState(false);
    const [searchValue, setSearchValue] = useState("");
    const [category, setCategory] = useState(entertainment);
    const [isCategoryPage, setIsCategoryPage] = useState(false);
    const [isAllCategoriesPage, setIsAllGategoriesPage] = useState(false);

    const [entertainmentArticles, setEntertainmentArticles] = useState([]);
    const [scienceArticles, setScienceArticles] = useState([]);
    const [generalArticles, setGeneralArticles] = useState([]);
    const [healthArticles, setHealthArticles] = useState([]);
    const [sportArticles, setSportArticles] = useState([]);
    const [technologyArticles, setTechnologyArticles] = useState([]);


    useEffect(() => {
        if (!location.pathname.includes("search")) {
            setSearch(false);
            setSearchValue("");
            if (urlParams.country && urlParams.country === gb.country) {
                setTopNewsText(gbNewsText);
                setCountry(gb.country);
            } else if (urlParams.country && urlParams.country === us.country) {
                setTopNewsText(usNewsText);
                setCountry(us.country);
            }
        }
        setUrl(`${newsapiUrl}q=${searchValue}&country=${country}&apiKey=${apiKey}`);
    }, [country, location.pathname, searchValue, urlParams.country]);

    useEffect(() => {
        if (location.pathname === "/search/country/gb") {
            setTopNewsText(gbSearchText);
            setCountry(gb.country);
            setSearch(true);
        } else if (location.pathname === "/search/country/us") {
            setTopNewsText(usSearchText);
            setCountry(us.country);
            setSearch(true);
        }
    }, [location]);

    useEffect(() => {
        if (location.pathname.includes("categories") && urlParams.category === "all") {
            getData(`${newsapiUrl}pageSize=5&page=1&category=${entertainment}&country=${country}&apiKey=${apiKey}`).then(res => setEntertainmentArticles(res.articles))
            getData(`${newsapiUrl}pageSize=5&page=1&category=${science}&country=${country}&apiKey=${apiKey}`).then(res => setScienceArticles(res.articles));
            getData(`${newsapiUrl}pageSize=5&page=1&category=${general}&country=${country}&apiKey=${apiKey}`).then(res => setGeneralArticles(res.articles))
            getData(`${newsapiUrl}pageSize=5&page=1&category=${health}&country=${country}&apiKey=${apiKey}`).then(res => setHealthArticles(res.articles))
            getData(`${newsapiUrl}pageSize=5&page=1&category=${sport}&country=${country}&apiKey=${apiKey}`).then(res => setSportArticles(res.articles))
            getData(`${newsapiUrl}pageSize=5&page=1&category=${technology}&country=${country}&apiKey=${apiKey}`).then(res => setTechnologyArticles(res.articles))
        }
    }, [isAllCategoriesPage, country, location.pathname, urlParams.category]);

    const getData = async (url) =>   {
        const response = await fetch(url);
        return await response.json();
    };

    const onSearchInputChange = (e) =>   {
        setSearchValue(e.target.value);
        setUrl(`${newsapiUrl}` +
            `q=${searchValue}&` +
            `country=${country}&` +
            `apiKey=${apiKey}`)
    };

    useEffect(() => {
        if (urlParams.category === "all") {
            setIsAllGategoriesPage(true);
        } else {
            setIsAllGategoriesPage(false);
            setTopNewsText(`Top ${category} news from ${country.toUpperCase()}`)
        }
        if (location.pathname.includes("categories")) {
            setCategory(urlParams.category === "all" ? ""  :   urlParams.category);
            //setTopNewsText(urlParams.country === gb.country ? gbCategoriesText    :   urlParams.country === us.country ? usCategoriesText   :   null);
            if (urlParams.category==="all") {
                setUrl(`${newsapiUrl}pageSize=5&page=1&category=${category}&country=${country}&apiKey=${apiKey}`);
            } else {
                setUrl(`${newsapiUrl}category=${category}&country=${country}&apiKey=${apiKey}`);
            }
            setIsCategoryPage(true);
        } else {
            setIsCategoryPage(false);
        }
    }, [category, country, location.pathname, urlParams.category, urlParams.country]);


    return (
        <div className="container mb-4">
            {search && <Search value={searchValue}
                               onChangeInput={onSearchInputChange}/>}
            <ul>
                <li><h4>{topNewsText}</h4></li>
            </ul>
            <div className="row">
                { !isCategoryPage && !loading && data.articles && data.articles.map((article, id) => {
                  return (
                      <div key={id} className="col-lg-4 col-md-6 col-sm-12" align="center">
                          <NewsArticleCard article={article}/>
                      </div>
                  )
                }) }

                {isCategoryPage && urlParams.category === "all" && <div className="border w-100 ml-5 mr-5">
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={entertainmentArticles} category={entertainment} counrty={country}/>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={scienceArticles} category={science} counrty={country}/>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={generalArticles} category={general} counrty={country}/>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={sportArticles} category={sport} counrty={country}/>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={healthArticles} category={health} counrty={country}/>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <CategorySlider articles={technologyArticles} category={technology} counrty={country}/>
                    </div>
                </div>}

                { isCategoryPage && urlParams.category !== "all" && !loading && data.articles && data.articles.map((article, id) => {
                    return (
                        <div key={id} className="col-lg-12 col-md-12 col-sm-12" align="center">
                            <NewsArticleCard article={article}/>
                        </div>
                    )
                }) }


                { loading && <h4 className="m-auto">Loading...</h4> }
            </div>
        </div>
    )
}

export default TopNews;