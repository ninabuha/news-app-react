export const countries = [{country: "gb", url: "/country/gb"}, {country: "us", url: "/country/us"}];

export const mainMenuItems = [{name: "Top News", url: "/topNews"}, {name: "Categories", url: "/categories/category/all"}, {name: "Search", url: "/search"}];