export const newsapiUrl = "http://newsapi.org/v2/top-headlines?";
export const apiKey = "2081c647da39438fb056de53b76089a4";

export const gbNewsText = "Top news from Great Britain";
export const usNewsText = "Top news from United States";

export const gbSearchText = "Search Top News From Great Britain by Term";
export const usSearchText = "Search Top News From United States by Term";

export const gbCategoriesText = "Top 5 news by categories from GB";
export const usCategoriesText = "Top 5 news by categories from US";
export const entertainment = "entertainment";
export const science = "science";
export const general = "general";
export const health = "health";
export const sport = "sport";
export const technology = "technology";